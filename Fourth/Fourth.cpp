﻿#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	const int N = 7;
	int mass[N][N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			mass[i][j] = i + j;
			cout << mass[i][j] << " ";
		}cout << "\n";
	}
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int date = buf.tm_mday;
	cout <<"Текущая дата: " << date << "\n";
	int LineNumber = date % N;
	cout<<"Остаток от деления: " << LineNumber << "\n";
	int summ=0;
	cout << "Строка массива: ";
	for (int j = 0; j < N; j++)
	{
		
		mass[LineNumber][j] = LineNumber + j;
		cout << mass[LineNumber][j] << " ";
		summ += mass[LineNumber][j];
	}cout <<"\n"<<"Сумма элементов строки: " << summ;
	
}